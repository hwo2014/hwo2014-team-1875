class Car
  attr_accessor :color, :name

  def initialize(car_hash)
    @name = car_hash["id"]
    @color = car_hash["color"]
  end

  def is_mine()
    return @color == MyCar.color
  end
end

class MyCar
  attr_accessor :color

  def initialize(car_hash)
    @@car = Car.new(car_hash)
  end

  def self.color
    @@car.color
  end
end