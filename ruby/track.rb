require_relative 'track_piece.rb'

class Track
  def initialize(track_hash)
    @id = track_hash["id"]
    @name = track_hash["name"]
    @track_pieces = Array.new
    track_hash["pieces"].each_with_index do |current_piece_hash, index|
      @track_pieces.push(TrackPiece.new(current_piece_hash, index))
    end
  end

  def compile(car_positions_hash)
    @track_pieces.each do |track_piece|
      track_piece.clear()
    end
    car_positions_hash.each do |car_position_hash|
      car = Car.new(car_position_hash["id"])
      current_track_piece_index = car_position_hash["piecePosition"]["pieceIndex"]
      current_track_piece_distance = car_position_hash["piecePosition"]["inPieceDistance"]

      current_track_piece = @track_pieces[current_track_piece_index]
      
      if car.is_mine
        @track_piece_my_car_is_on = current_track_piece
      end

      current_track_piece.add_car(car, current_track_piece_distance)
    end
  end

  def render()
    puts "\e[H\e[2J"
    puts "track pieces: #{@track_pieces.count}"
    @track_pieces.each do |track_piece|
      track_piece.render()
    end
  end
end