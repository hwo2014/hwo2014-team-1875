class TrackPiece
  attr_accessor :index

  def initialize(track_piece_hash, index)
    @length = track_piece_hash["length"]
    @switch = track_piece_hash["switch"] == 'true'
    @angle = track_piece_hash["angle"]
    @index = index
    @cars = Array.new
  end

  def is_curve()
    return !@angle.nil?
  end

  def can_switch_lanes()
    return @switch
  end

  def add_car(car, distance)
    @cars.push([car, distance])
  end

  def clear()
    @cars.clear
  end

  def render()
    puts "track #{@index}"
    @cars.each do |car|
      if is_curve
        puts "    car #{car[0].color} on curve..."
      else
        puts "    car #{car[0].color} - #{car[1] / @length}%"   
      end
    end
  end
end